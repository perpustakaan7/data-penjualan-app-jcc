import BaseSchema from "@ioc:Adonis/Lucid/Schema";

export default class Datapenjualans extends BaseSchema {
  protected tableName = "penjualans";

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments("id");
      table.string("namabarang").notNullable();
      table.integer("stok").notNullable();
      table.integer("terjual").notNullable();
      table.date("transaksi").notNullable();
      table.string("jenisbarang").notNullable();
      table.timestamps(true, true);
    });
  }

  public async down() {
    this.schema.dropTable(this.tableName);
  }
}
