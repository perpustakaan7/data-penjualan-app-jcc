/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer'
|
*/

import Route from "@ioc:Adonis/Core/Route";

Route.get("/", async ({ response }) => {
  response.redirect().toPath("/docs/");
});

Route.group(() => {
  Route.resource("data-penjualan", "PenjualansController").apiOnly();
  Route.post("/search/nama-barang", "SearchingsController.searchNamaBarang");
  Route.post(
    "/search/tanggal-transaksi",
    "SearchingsController.searchTanggalTransaksi"
  );
  Route.post("/search/jenis-barang", "SearchingsController.filterJenisBarang");
  Route.post("/search/date-range", "SearchingsController.filterDate");
  Route.get("/search", "SearchingsController.search");
  Route.get("/search/filter-tertinggi", "SearchingsController.filterTertinggi");
  Route.get("/search/filter-terendah", "SearchingsController.filterTerendah");
  Route.post(
    "/search/date-range/tertinggi",
    "SearchingsController.filterDateTertinggi"
  );
  Route.post(
    "/search/date-range/terendah",
    "SearchingsController.filterDateTerendah"
  );
}).prefix("/api/v1");
