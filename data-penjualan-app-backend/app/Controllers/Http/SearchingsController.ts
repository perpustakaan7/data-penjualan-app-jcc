import type { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import Penjualan from "App/Models/Penjualan";

export default class SearchingsController {
  public async search({ response, request }: HttpContextContract) {
    /**
     * @swagger
     * /api/v1/search:
     *  get:
     *    tags:
     *      - Filter / Search
     *    summary: Search berdasarkan nama barang atau tanggal transaksi
     *    description: Endpoint untuk search berdasarkan nama barang atau tanggal transaksi
     *    parameters:
     *      - in: query
     *        name: namabarang
     *        schema:
     *          type: string
     *        description: Nama Barang
     *      - in: query
     *        name: transaksi
     *        schema:
     *          type: dateTime
     *        description: Tanggal Transaksi
     *    responses:
     *      "200":
     *        description: Success
     */
    if (Object.keys(request.qs()).length === 0) {
      let data = await Penjualan.all();
      return response.status(200).json({
        message: "Success",
        data,
      });
    } else if (request.qs().namabarang) {
      let data = await Penjualan.query().where(
        "namabarang",
        request.qs().namabarang
      );
      if (data.length === 0) {
        return response.status(400).json({
          message: "Mohon maaf data tidak ada",
        });
      }
      return response.status(200).json({
        message: "Success",
        data,
      });
    } else if (request.qs().transaksi) {
      let data = await Penjualan.query().where(
        "transaksi",
        request.qs().transaksi
      );
      if (data.length === 0) {
        return response.status(400).json({
          message: "Mohon maaf data tidak ada",
        });
      }
      return response.status(200).json({
        message: "Success",
        data,
      });
    }
  }

  public async searchNamaBarang({ request, response }: HttpContextContract) {
    /**
     * @swagger
     * /api/v1/search/nama-barang:
     *  post:
     *    tags:
     *      - Filter / Search
     *    summary: Search berdasarkan nama barang
     *    description: Endpoint untuk search berdasarkan nama barang
     *    requestBody:
     *      content:
     *        application/x-www-form-urlencoded:
     *          schema:
     *            type: object
     *            properties:
     *              namabarang:
     *                type: string
     *                example: Kopi
     *        application/json:
     *          schema:
     *            type: object
     *            properties:
     *              namabarang:
     *                type: string
     *                example: Kopi
     *    responses:
     *      "200":
     *        description: Berhasil mendapatkan data
     */
    let data = await Penjualan.query().where(
      "namabarang",
      request.input("namabarang")
    );

    if (data.length === 0) {
      return response.status(400).json({
        message: `Mohon maaf barang dengan nama: ${request.input(
          "namabarang"
        )} tidak terdaftar`,
      });
    }
    return response.ok({
      message: `Berhasil mendapatkan data dengan nama: ${request.input(
        "namabarang"
      )}`,
      data,
    });
  }

  public async searchTanggalTransaksi({
    request,
    response,
  }: HttpContextContract) {
    /**
     * @swagger
     * /api/v1/search/tanggal-transaksi:
     *  post:
     *    tags:
     *      - Filter / Search
     *    summary: Search berdasarkan tanggal transaksi
     *    description: Endpoint untuk search berdasarkan tanggal transaksi
     *    requestBody:
     *      content:
     *        application/x-www-form-urlencoded:
     *          schema:
     *            type: object
     *            properties:
     *              tanggalTransaksi:
     *                type: dateTime
     *                example: 2022-04-10
     *        application/json:
     *          schema:
     *            type: object
     *            properties:
     *              tanggalTransaksi:
     *                type: dateTime
     *                example: 2022-04-10
     *    responses:
     *      "200":
     *        description: Berhasil mendapatkan data
     */
    let data = await Penjualan.query().where(
      "transaksi",
      request.input("tanggalTransaksi")
    );

    if (data.length === 0) {
      return response.status(400).json({
        message: `Mohon maaf barang pada tanggal: ${request.input(
          "tanggalTransaksi"
        )} tidak ada`,
      });
    }
    return response.ok({
      message: `Berhasil mendapatkan data barang pada tanggal: ${request.input(
        "tanggalTransaksi"
      )}`,
      data,
    });
  }

  public async filterJenisBarang({ response, request }: HttpContextContract) {
    /**
     * @swagger
     * /api/v1/search/jenis-barang:
     *  post:
     *    tags:
     *      - Filter / Search
     *    summary: Filter berdasarkan jenis barang
     *    description: Endpoint untuk filter berdasarkan jenis barang
     *    requestBody:
     *      content:
     *        application/x-www-form-urlencoded:
     *          schema:
     *            type: object
     *            properties:
     *              jenisbarang:
     *                type: string
     *                example: konsumsi
     *        application/json:
     *          schema:
     *            type: object
     *            properties:
     *              jenisbarang:
     *                type: string
     *                example: konsumsi
     *    responses:
     *      "200":
     *        description: Berhasil mendapatkan data jenis barang
     */
    let data = await Penjualan.query().where(
      "jenisbarang",
      request.input("jenisbarang")
    );
    if (data.length === 0) {
      return response.status(400).json({
        message: "Mohon maaf data tidak ada",
      });
    }
    return response.status(200).json({
      message: "Berhasil mendapatkan data jenis barang",
      data,
    });
  }

  public async filterTertinggi({ response, request }: HttpContextContract) {
    /**
     * @swagger
     * /api/v1/search/filter-tertinggi:
     *  get:
     *    tags:
     *      - Filter / Search
     *    summary: Filter berdasarkan terjual tertinggi
     *    description: Endpoint untuk filter berdasarkan terjual tertinggi
     *    parameters:
     *      - in: query
     *        name: jenisBarang
     *        schema:
     *          type: string
     *        description: Jenis Barang
     *    responses:
     *      "200":
     *        description: Success
     */
    if (Object.keys(request.qs()).length === 0) {
      let data = await Penjualan.query().orderBy("terjual", "desc");
      return response.status(200).json({
        message: "Success",
        data,
      });
    } else {
      let data = await Penjualan.query()
        .orderBy("terjual", "asc")
        .where("jenisbarang", request.qs().jenisBarang);
      if (data.length === 0) {
        return response.status(400).json({
          message: "Mohon maaf data tidak ada",
        });
      }
      return response.status(200).json({
        message: "Success",
        data,
      });
    }
  }

  public async filterTerendah({ response, request }: HttpContextContract) {
    /**
     * @swagger
     * /api/v1/search/filter-terendah:
     *  get:
     *    tags:
     *      - Filter / Search
     *    summary: Filter berdasarkan terjual terendah
     *    description: Endpoint untuk filter berdasarkan terjual terendah
     *    parameters:
     *      - in: query
     *        name: jenisBarang
     *        schema:
     *          type: string
     *        description: Jenis Barang
     *    responses:
     *      "200":
     *        description: Success
     */
    if (Object.keys(request.qs()).length === 0) {
      let data = await Penjualan.query().orderBy("terjual", "asc");
      return response.status(200).json({
        message: "Success",
        data,
      });
    } else {
      let data = await Penjualan.query()
        .orderBy("terjual", "asc")
        .where("jenisbarang", request.qs().jenisBarang);
      if (data.length === 0) {
        return response.status(400).json({
          message: "Mohon maaf data tidak ada",
        });
      }
      return response.status(200).json({
        message: "Success",
        data,
      });
    }
  }

  public async filterDate({ request, response }: HttpContextContract) {
    /**
     * @swagger
     * /api/v1/search/date-range:
     *  post:
     *    tags:
     *      - Filter / Search
     *    summary: Filter berdasarkan rentang waktu
     *    description: Endpoint untuk filter berdasarkan rentang waktu
     *    requestBody:
     *      content:
     *        application/x-www-form-urlencoded:
     *          schema:
     *            type: object
     *            properties:
     *              startDate:
     *                type: dateTime
     *                example: 2022-04-08
     *              endDate:
     *                type: dateTime
     *                example: 2022-04-09
     *        application/json:
     *          schema:
     *            type: object
     *            properties:
     *              startDate:
     *                type: dateTime
     *                example: 2022-04-08
     *              endDate:
     *                type: dateTime
     *                example: 2022-04-09
     *    responses:
     *      "200":
     *        description: Berhasil mendapatkan data rentang waktu
     */
    let data = await Penjualan.query().whereBetween("transaksi", [
      request.input("startDate"),
      request.input("endDate"),
    ]);
    return response.status(200).json({
      message: "Berhasil mendapatkan data rentang waktu",
      data,
    });
  }

  public async filterDateTertinggi({ request, response }: HttpContextContract) {
    /**
     * @swagger
     * /api/v1/search/date-range/tertinggi:
     *  post:
     *    tags:
     *      - Filter / Search
     *    summary: Filter berdasarkan rentang waktu dengan terjual tertinggi
     *    description: Endpoint untuk filter berdasarkan rentang waktu dengan terjual tertinggi
     *    parameters:
     *      - in: query
     *        name: jenisBarang
     *        schema:
     *          type: string
     *        description: Jenis Barang
     *    requestBody:
     *      content:
     *        application/x-www-form-urlencoded:
     *          schema:
     *            type: object
     *            properties:
     *              startDate:
     *                type: dateTime
     *                example: 2022-04-08
     *              endDate:
     *                type: dateTime
     *                example: 2022-04-09
     *        application/json:
     *          schema:
     *            type: object
     *            properties:
     *              startDate:
     *                type: dateTime
     *                example: 2022-04-08
     *              endDate:
     *                type: dateTime
     *                example: 2022-04-09
     *    responses:
     *      "200":
     *        description: Berhasil mendapatkan data
     *      "400":
     *        description: Mohon maaf endDate harus lebih dari startDate
     */
    if (Object.keys(request.qs()).length === 0) {
      if (request.input("endDate") < request.input("startDate")) {
        return response.status(400).json({
          message: "Mohon maaf endDate harus lebih dari startDate",
        });
      }
      let data = await Penjualan.query().whereBetween("transaksi", [
        request.input("startDate"),
        request.input("endDate"),
      ]);
      if (data.length === 0) {
        return response.status(400).json({
          message: "Mohon maaf data tidak ada",
        });
      }
      return response.status(200).json({
        message: "Berhasil mendapatkan data rentang waktu",
        data,
      });
    } else {
      let data = await Penjualan.query()
        .whereBetween("transaksi", [
          request.input("startDate"),
          request.input("endDate"),
        ])
        .orderBy("terjual", "desc")
        .where("jenisbarang", request.qs().jenisBarang);
      if (data.length === 0) {
        return response.status(400).json({
          message: "Mohon maaf data tidak ada",
        });
      }
      return response.status(200).json({
        message: "Berhasil mendapatkan data",
        data,
      });
    }
  }

  public async filterDateTerendah({ request, response }: HttpContextContract) {
    /**
     * @swagger
     * /api/v1/search/date-range/terendah:
     *  post:
     *    tags:
     *      - Filter / Search
     *    summary: Filter berdasarkan rentang waktu dengan terjual terendah
     *    description: Endpoint untuk filter berdasarkan rentang waktu dengan terjual terendah
     *    parameters:
     *      - in: query
     *        name: jenisBarang
     *        schema:
     *          type: string
     *        description: Jenis Barang
     *    requestBody:
     *      content:
     *        application/x-www-form-urlencoded:
     *          schema:
     *            type: object
     *            properties:
     *              startDate:
     *                type: dateTime
     *                example: 2022-04-08
     *              endDate:
     *                type: dateTime
     *                example: 2022-04-09
     *        application/json:
     *          schema:
     *            type: object
     *            properties:
     *              startDate:
     *                type: dateTime
     *                example: 2022-04-08
     *              endDate:
     *                type: dateTime
     *                example: 2022-04-09
     *    responses:
     *      "200":
     *        description: Berhasil mendapatkan data
     *      "400":
     *        description: Mohon maaf endDate harus lebih dari startDate
     */
    if (Object.keys(request.qs()).length === 0) {
      if (request.input("endDate") < request.input("startDate")) {
        return response.status(400).json({
          message: "Mohon maaf endDate harus lebih dari startDate",
        });
      }
      let data = await Penjualan.query().whereBetween("transaksi", [
        request.input("startDate"),
        request.input("endDate"),
      ]);
      if (data.length === 0) {
        return response.status(400).json({
          message: "Mohon maaf data tidak ada",
        });
      }
      return response.status(200).json({
        message: "Berhasil mendapatkan data rentang waktu",
        data,
      });
    } else {
      let data = await Penjualan.query()
        .whereBetween("transaksi", [
          request.input("startDate"),
          request.input("endDate"),
        ])
        .orderBy("terjual", "asc")
        .where("jenisbarang", request.qs().jenisBarang);
      if (data.length === 0) {
        return response.status(400).json({
          message: "Mohon maaf data tidak ada",
        });
      }
      return response.status(200).json({
        message: "Berhasil mendapatkan data",
        data,
      });
    }
  }
}
