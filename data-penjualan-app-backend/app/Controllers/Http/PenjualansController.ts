import type { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import Penjualan from "App/Models/Penjualan";
import PenjualanValidator from "App/Validators/PenjualanValidator";

export default class PenjualansController {
  public async store({ request, response }: HttpContextContract) {
    /**
     * @swagger
     * /api/v1/data-penjualan:
     *  post:
     *    tags:
     *      - Data Penjualan
     *    summary: Tambah Data Penjualan
     *    description: Endpoint untuk tambah data penjualan
     *    requestBody:
     *      content:
     *        application/x-www-form-urlencoded:
     *          schema:
     *            $ref: '#/definitions/Penjualan'
     *        application/json:
     *          schema:
     *            $ref: '#/definitions/Penjualan'
     *    responses:
     *      "200":
     *        description: success create data
     */
    let data = await request.validate(PenjualanValidator);
    await Penjualan.create(data);
    return response.created({ message: "success create data", data });
  }

  public async index({ response }: HttpContextContract) {
    /**
     * @swagger
     * /api/v1/data-penjualan:
     *  get:
     *    tags:
     *      - Data Penjualan
     *    summary: tampilkan seluruh data penjualan
     *    description: endpoint untuk tampilkan seluruh data penjualan
     *    responses:
     *      "200":
     *        description: Tampilan seluruh data penjualan
     */
    let data = await Penjualan.all();
    return response.ok({ message: "Berhasil tampilkan seluruh data", data });
  }

  public async show({ params, response }: HttpContextContract) {
    /**
     * @swagger
     * /api/v1/data-penjualan/{id}:
     *  get:
     *    tags:
     *      - Data Penjualan
     *    summary: tampilkan data penjualan berdasarkan id
     *    description: endpoint untuk menampilkan data penjualan berdasarkan id
     *    parameters:
     *      - in: path
     *        name: id
     *        required: true
     *        schema:
     *          type: integer
     *          minimum: 1
     *          example: 1
     *        description: id data penjualan
     *    responses:
     *      "200":
     *        description: Berhasil tampilkan data berdasarkan id
     */
    let data = await Penjualan.findByOrFail("id", params.id);
    return response.ok({
      message: "Berhasil tampilkan data berdasarkan id",
      data,
    });
  }

  public async update({ params, response, request }: HttpContextContract) {
    /**
     * @swagger
     * /api/v1/data-penjualan/{id}:
     *  put:
     *    tags:
     *      - Data Penjualan
     *    summary: Update data penjualan berdasarkan id
     *    description: Endpoint untuk update data penjualan berdasarkan id
     *    parameters:
     *      - in: path
     *        name: id
     *        required: true
     *        schema:
     *          type: integer
     *          minimum: 1
     *          example: 1
     *        description: id data penjualan
     *    requestBody:
     *      content:
     *        application/x-www-form-urlencoded:
     *          schema:
     *            type: object
     *            properties:
     *              namabarang:
     *                type: string
     *                example: Kopi
     *              stok:
     *                type: number
     *                example: 100
     *              terjual:
     *                type: number
     *                example: 100
     *              transaksi:
     *                type: dateTime
     *                example: 2022-04-11
     *              jenisbarang:
     *                type: string
     *                example: konsumsi
     *        application/json:
     *          schema:
     *            type: object
     *            properties:
     *              namabarang:
     *                type: string
     *                example: Kopi
     *              stok:
     *                type: number
     *                example: 100
     *              terjual:
     *                type: number
     *                example: 100
     *              transaksi:
     *                type: dateTime
     *                example: 2022-04-11
     *              jenisbarang:
     *                type: string
     *                example: konsumsi
     *    responses:
     *      "200":
     *        description: Berhasil update data penjualan
     */
    let data = await Penjualan.findByOrFail("id", params.id);
    data.namabarang = request.input("namabarang");
    data.stok = request.input("stok");
    data.terjual = request.input("terjual");
    data.transaksi = request.input("transaksi");
    data.jenisbarang = request.input("jenisbarang");
    await data.save();

    return response.status(200).json({
      message: `Berhasil update data dengan id: ${params.id}`,
    });
  }

  public async destroy({ params, response }: HttpContextContract) {
    /**
     * @swagger
     * /api/v1/data-penjualan/{id}:
     *  delete:
     *    tags:
     *      - Data Penjualan
     *    summary: Delete data penjualan berdasarkan id
     *    description: Endpoint untuk delete data penjualan berdasarkan id
     *    parameters:
     *      - in: path
     *        name: id
     *        required: true
     *        schema:
     *          type: integer
     *          minimum: 1
     *          example: 1
     *        description: id data penjualan
     *    responses:
     *      "200":
     *        description: Berhasil hapus data penjualan
     */
    let data = await Penjualan.findByOrFail("id", params.id);
    await data.delete();

    return response.status(200).json({
      message: `Berhasil hapus data dengan id: ${params.id}`,
    });
  }
}
