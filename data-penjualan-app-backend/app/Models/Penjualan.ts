import { DateTime } from "luxon";
import { BaseModel, column } from "@ioc:Adonis/Lucid/Orm";

export default class Penjualan extends BaseModel {
  /**
   * @swagger
   * definitions:
   *  Penjualan:
   *    type: object
   *    properties:
   *      namabarang:
   *        type: string
   *        example: Kopi
   *      stok:
   *        type: number
   *        example: 100
   *      terjual:
   *        type: number
   *        example: 100
   *      transaksi:
   *        type: dateTime
   *        example: 2022-04-10
   *      jenisbarang:
   *        type: string
   *        example: konsumsi
   *    required:
   *      - namabarang
   *      - stok
   *      - terjual
   *      - transaksi
   *      - jenisbarang
   */

  @column({ isPrimary: true })
  public id: number;

  @column()
  public namabarang: string;

  @column()
  public stok: number;

  @column()
  public terjual: number;

  @column.date()
  public transaksi: DateTime;

  @column()
  public jenisbarang: string;

  @column.dateTime({ autoCreate: true, serializeAs: null })
  public createdAt: DateTime;

  @column.dateTime({ autoCreate: true, autoUpdate: true, serializeAs: null })
  public updatedAt: DateTime;
}
