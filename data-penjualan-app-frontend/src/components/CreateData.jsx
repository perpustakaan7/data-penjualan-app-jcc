import React from 'react';
import { useState } from 'react';
import axios from 'axios';

export default function CreateData() {
  const [namaBarang, setNamaBarang] = useState('');
  const [stok, setStok] = useState('');
  const [jumlahTerjual, setJumlahTejual] = useState('');
  const [tanggalTransaksi, setTanggalTransaksi] = useState('');
  const [jenisBarang, setJenisBarang] = useState('');

  function saveDataPenjualan(e) {
    axios({
      method: 'POST',
      url: 'https://data-penjualan-api.herokuapp.com/api/v1/data-penjualan',
      data: {
        namabarang: namaBarang,
        stok,
        terjual: jumlahTerjual,
        transaksi: tanggalTransaksi,
        jenisbarang: jenisBarang,
      },
    }).then(result => {
      if (result.data) {
        alert('data berhasil ditambahkan !');
        window.location.href = '/data-penjualan';
        // window.location.reload();
      } else {
        alert('data gagal ditambahkan!');
        window.location.reload();
      }
    });
  }

  return (
    <div className="min-h-full flex items-center justify-center py-12 px-4 sm:px-6 lg:px-8">
      <div className="max-w-md w-full space-y-8">
        <div>
          <img
            className="mx-auto h-12 w-auto"
            src="https://tailwindui.com/img/logos/workflow-mark-indigo-600.svg"
            alt="Workflow"
          />
          <h2 className="mt-6 text-center text-3xl font-extrabold text-gray-900">
            Tambah Data Penjualan
          </h2>
        </div>
        <input type="hidden" name="remember" value="true" />
        <div className="shadow-sm -space-y-px">
          <div>
            <label htmlFor="namabarang" className="sr-only">
              Nama Barang
            </label>
            <input
              id="namabarang"
              name="namabarang"
              type="text"
              required
              className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
              placeholder="Nama Barang"
              value={namaBarang}
              onChange={e => setNamaBarang(e.target.value)}
            />
          </div>
          <div>
            <label htmlFor="stok" className="sr-only">
              Stok
            </label>
            <input
              id="stok"
              name="stok"
              type="text"
              required
              className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
              placeholder="Stok"
              value={stok}
              onChange={e => setStok(e.target.value)}
            />
          </div>
          <div>
            <label htmlFor="terjual" className="sr-only">
              Jumlah Terjual
            </label>
            <input
              id="terjual"
              name="terjual"
              type="text"
              required
              className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
              placeholder="Jumlah Terjual"
              value={jumlahTerjual}
              onChange={e => setJumlahTejual(e.target.value)}
            />
          </div>
          <div>
            <label htmlFor="transaksi" className="sr-only">
              Tanggal Transaksi
            </label>
            <input
              id="transaksi"
              name="transaksi"
              type="text"
              required
              className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
              placeholder="Tanggal Transaksi: format tanggal: yyyy-MM-DD"
              value={tanggalTransaksi}
              onChange={e => setTanggalTransaksi(e.target.value)}
            />
          </div>
          <div>
            <label htmlFor="barang" className="sr-only">
              Jenis Barang
            </label>
            <input
              id="barang"
              name="barang"
              type="text"
              required
              className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm"
              placeholder="Jenis Barang"
              value={jenisBarang}
              onChange={e => setJenisBarang(e.target.value)}
            />
          </div>
        </div>
        <button
          onClick={saveDataPenjualan}
          type="submit"
          className="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
        >
          Tambah Data Penjualan
        </button>
      </div>
    </div>
  );
}
