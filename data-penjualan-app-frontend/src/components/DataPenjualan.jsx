import React, { useEffect, useState } from 'react';
import './dataPenjualan.css';
import axios from 'axios';


export default function DataPenjualan() {
  const [data, setData] = useState([]);
  const [dataJenisBarang, setDataJenisBarang] = useState([]);
  const [filterData, setFilterData] = useState([]);
  const [filterJumlahTerjual, setFilterJumlahTerjual] = useState('');
  const [filterJenisBarang, setFilterJenisBarang] = useState('');
  const [startDate, setStartDate] = useState('')
  const [endDate, setEndDate] = useState('')

  const sortedData = data.sort(function (a, b) {
    return a.id - b.id;
  });

  const handleChangeFilterJumlahTerjual = e => {
    setFilterJumlahTerjual(e.target.value);
  };

  const handleChangeFilterJenisBarang = e => {
    setFilterJenisBarang(e.target.value);
  };

  const handleStartDate = e => {
    setStartDate(e.target.value)
  }

  const handleEndDate = e => {
    setEndDate(e.target.value)
  }

  if (filterJumlahTerjual === 'tertinggi') {
    data.sort(function (a, b) {
      return b.terjual - a.terjual;
    });
  } else if (filterJumlahTerjual === 'terendah') {
    data.sort(function (a, b) {
      return a.terjual - b.terjual;
    });
  }

  const handleResetFilter = () => {
    setFilterJumlahTerjual('');
    setFilterJenisBarang('');
    setStartDate('');
    setEndDate('');
  };

  if (endDate < startDate && endDate !== "") {
    alert("Waktu terakhir harus lebih dari waktu awal")
  }

  useEffect(() => {
    if (endDate !== "" && startDate !== "") {
      axios({
        method: 'POST',
        url: 'https://data-penjualan-api.herokuapp.com/api/v1/search/date-range',
        data: {
          startDate,
          endDate
        },
      }).then(result => {
        setData(result.data.data)
      });
    } else {
      axios({
        method: 'GET',
        url: 'https://data-penjualan-api.herokuapp.com/api/v1/data-penjualan',
      }).then(results => {
        setData(results.data.data);
        const jenisBarang = [];
        for (let i = 0; i < results.data.data.length; i++) {
          jenisBarang.push(results.data.data[i].jenisbarang.toLowerCase());
        }
        const unique = [...new Set(jenisBarang)];
        setDataJenisBarang(unique);
      });
    }
  }, [startDate, endDate]);

  const deleteProduct = id => {
    axios({
      method: 'delete',
      url: `https://data-penjualan-api.herokuapp.com/api/v1/data-penjualan/${id}`,
    }).then(result => {
      if (!result.data.affectedRows) {
        alert('data berhasil di hapus !');
        window.location.reload();
      } else {
        alert('data gagal ditambahkan!');
        window.location.reload();
      }
    });
  };

  return (
    <div className="container mt-4">
      <div className="grid grid-cols-6 gap-4 min-h-screen bg-white">
        <div className="w-full shadow p-5 rounded-lg bg-white col-span-2">
          <div className="relative">
            <div className="absolute flex items-center ml-2 h-full">
              <svg
                className="w-4 h-4 fill-current text-primary-gray-dark"
                viewBox="0 0 16 16"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path d="M15.8898 15.0493L11.8588 11.0182C11.7869 10.9463 11.6932 10.9088 11.5932 10.9088H11.2713C12.3431 9.74952 12.9994 8.20272 12.9994 6.49968C12.9994 2.90923 10.0901 0 6.49968 0C2.90923 0 0 2.90923 0 6.49968C0 10.0901 2.90923 12.9994 6.49968 12.9994C8.20272 12.9994 9.74952 12.3431 10.9088 11.2744V11.5932C10.9088 11.6932 10.9495 11.7869 11.0182 11.8588L15.0493 15.8898C15.1961 16.0367 15.4336 16.0367 15.5805 15.8898L15.8898 15.5805C16.0367 15.4336 16.0367 15.1961 15.8898 15.0493ZM6.49968 11.9994C3.45921 11.9994 0.999951 9.54016 0.999951 6.49968C0.999951 3.45921 3.45921 0.999951 6.49968 0.999951C9.54016 0.999951 11.9994 3.45921 11.9994 6.49968C11.9994 9.54016 9.54016 11.9994 6.49968 11.9994Z"></path>
              </svg>
            </div>
            <input
              type="text"
              placeholder="Search dari Nama Barang atau Tanggal Transaksi"
              value={filterData}
              onChange={e => setFilterData(e.target.value)}
              className="px-8 py-3 w-full rounded-md bg-gray-100 border-transparent focus:border-gray-500 focus:bg-white focus:ring-0 text-sm"
            />
          </div>
          <div className="flex items-center justify-between mt-4">
            <p className="font-medium">Filters</p>
            <button
              onClick={handleResetFilter}
              className="px-4 py-2 bg-gray-100 hover:bg-gray-200 text-gray-800 text-sm font-medium rounded-md"
            >
              Reset Filter
            </button>
          </div>
          <div>
            <div className="grid grid-cols-4 gap-4 mt-4">
              <select
                value={filterJenisBarang}
                onChange={handleChangeFilterJenisBarang}
                className="col-span-2 px-4 py-3 w-full rounded-md bg-gray-100 border-transparent focus:border-gray-500 focus:bg-white focus:ring-0 text-sm"
              >
                <option value="">Jenis Barang</option>
                {dataJenisBarang.map((item, i) => {
                  return (
                    <option key={i} value={item}>
                      {item}
                    </option>
                  );
                })}
              </select>
              <select
                value={filterJumlahTerjual}
                onChange={handleChangeFilterJumlahTerjual}
                className="col-span-2 px-4 py-3 w-full rounded-md bg-gray-100 border-transparent focus:border-gray-500 focus:bg-white focus:ring-0 text-sm"
              >
                <option value="">Jumlah Terjual</option>
                <option value="tertinggi">Tertinggi</option>
                <option value="terendah">Terendah</option>
              </select>
            </div>
          </div>
          <div className="mt-4">
            <div className="flex items-center">
              <input value={startDate} onChange={handleStartDate} type="date" name="start" className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Select date start" />
              <span className="mx-4 text-gray-500">to</span>
              <input value={endDate} onChange={handleEndDate} name="end" type="date" className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Select date end" />
            </div>
          </div>
        </div>
        <div className="col-span-4">
          <div className="overflow-auto lg:overflow-visible">
            <div className="flex lg:justify-between border-b-2 border-fuchsia-900 pb-1">
              <h2 className="text-2xl text-gray-500 font-bold">
                List Data Penjualan
              </h2>
              <div>
                <a href="/add-data-penjualan">
                  <button className="bg-blue-500 hover:bg-blue-700 text-white py-2 px-5 sm rounded-full">
                    Tambah Data Penjualan
                  </button>
                </a>
              </div>
            </div>
            <table className="table text-gray-400 border-separate space-y-6 text-sm w-full">
              <thead className="bg-blue-500 text-white">
                <tr>
                  <th className="p-3">ID</th>
                  <th className="p-3 text-left">Nama Barang</th>
                  <th className="p-3 text-left">Stok</th>
                  <th className="p-3 text-left">Jumlah Terjual</th>
                  <th className="p-3 text-left">Tanggal Transaksi</th>
                  <th className="p-3 text-left">Jenis Barang</th>
                  <th className="p-3 text-left">Action</th>
                </tr>
              </thead>
              <tbody>
                {filterJenisBarang === ''
                  ? filterData === ''
                    ? sortedData.map(item => (
                      <tr key={item.id} className="bg-blue-200 lg:text-black">
                        <td className="p-3">{item.id}</td>
                        <td className="p-3 font-medium capitalize">
                          {item.namabarang}
                        </td>
                        <td className="p-3">{item.stok}</td>
                        <td className="p-3">{item.terjual}</td>
                        <td className="p-3">{item.transaksi}</td>
                        <td className="p-3">{item.jenisbarang.toLowerCase()}</td>
                        <td className="p-3">
                          <div className="flex flex-row">
                            <button
                              onClick={() =>
                                (window.location.href = `/edit-data-penjualan/${item.id}`)
                              }
                              type="submit"
                              className="material-icons-outlined text-base text-gray-500 hover:text-black mr-2 cursor-pointer"
                            >
                              edit
                            </button>
                            <button
                              onClick={() => {
                                if (
                                  window.confirm(
                                    'Apakah anda yakin untuk menghapus ini ?'
                                  ) === true
                                ) {
                                  deleteProduct(item.id);
                                } else {
                                }
                              }}
                              type="submit"
                              className="material-icons-outlined text-base text-blue-500 hover:text-black mr-2 cursor-pointer"
                            >
                              delete
                            </button>
                          </div>
                        </td>
                      </tr>
                    ))
                    : sortedData
                      .filter(
                        item =>
                        (item.namabarang
                          .toLowerCase()
                          .includes(filterData) ||
                          item.transaksi.toLowerCase().includes(filterData))
                      )
                      .map(item => (
                        <tr
                          key={item.id}
                          className="bg-blue-200 lg:text-black"
                        >
                          <td className="p-3">{item.id}</td>
                          <td className="p-3 font-medium capitalize">
                            {item.namabarang}
                          </td>
                          <td className="p-3">{item.stok}</td>
                          <td className="p-3">{item.terjual}</td>
                          <td className="p-3">{item.transaksi}</td>
                          <td className="p-3">{item.jenisbarang.toLowerCase()}</td>
                          <td className="p-3">
                            <div className="flex flex-row">
                              <button
                                onClick={() =>
                                  (window.location.href = `/edit-data-penjualan/${item.id}`)
                                }
                                type="submit"
                                className="material-icons-outlined text-base text-gray-500 hover:text-black mr-2 cursor-pointer"
                              >
                                edit
                              </button>
                              <button
                                onClick={() => {
                                  if (
                                    window.confirm(
                                      'Apakah anda yakin untuk menghapus ini ?'
                                    ) === true
                                  ) {
                                    deleteProduct(item.id);
                                  } else {
                                  }
                                }}
                                type="submit"
                                className="material-icons-outlined text-base text-blue-500 hover:text-black mr-2 cursor-pointer"
                              >
                                delete
                              </button>
                            </div>
                          </td>
                        </tr>
                      ))
                  : sortedData
                    .filter(item =>
                      item.jenisbarang
                        .toLowerCase()
                        .includes(filterJenisBarang)
                    )
                    .map(item => (
                      <tr key={item.id} className="bg-blue-200 lg:text-black">
                        <td className="p-3">{item.id}</td>
                        <td className="p-3 font-medium capitalize">
                          {item.namabarang}
                        </td>
                        <td className="p-3">{item.stok}</td>
                        <td className="p-3">{item.terjual}</td>
                        <td className="p-3">{item.transaksi}</td>
                        <td className="p-3">{item.jenisbarang.toLowerCase()}</td>
                        <td className="p-3">
                          <div className="flex flex-row">
                            <button
                              onClick={() =>
                                (window.location.href = `/edit-data-penjualan/${item.id}`)
                              }
                              type="submit"
                              className="material-icons-outlined text-base text-gray-500 hover:text-black mr-2 cursor-pointer"
                            >
                              edit
                            </button>
                            <button
                              onClick={() => {
                                if (
                                  window.confirm(
                                    'Apakah anda yakin untuk menghapus ini ?'
                                  ) === true
                                ) {
                                  deleteProduct(item.id);
                                } else {
                                }
                              }}
                              type="submit"
                              className="material-icons-outlined text-base text-blue-500 hover:text-black mr-2 cursor-pointer"
                            >
                              delete
                            </button>
                          </div>
                        </td>
                      </tr>
                    ))}
              </tbody>
            </table>
          </div>
        </div>
      </div >
    </div >
  );
}
