import { Link } from 'react-router-dom';

function App() {
  return (
    <>
      <div className="min-h-full flex items-center justify-center py-12 px-4 sm:px-6 lg:px-8">
        <h1 className="mt-6 text-center text-3xl font-extrabold text-gray-900">
          Selamat Datang di Aplikasi Data Penjualan
        </h1>
      </div>
      <div className="min-h-full flex items-center justify-center py-12 px-4 sm:px-6 lg:px-8">
        <Link
          to={'/add-data-penjualan'}
          className="group relative w-100 py-2 mx-4 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
        >
          Tambah Data Penjualan
        </Link>
        <Link
          to={'/data-penjualan'}
          className="group relative w-100 py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
        >
          List Data Penjualan
        </Link>
      </div>
    </>
  );
}

export default App;
