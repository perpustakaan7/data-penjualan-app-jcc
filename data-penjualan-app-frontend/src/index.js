import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import "./index.css"
import { BrowserRouter, Routes, Route } from "react-router-dom";
import CreateData from './components/CreateData';
import DataPenjualan from './components/DataPenjualan';
import EditDataPenjualan from './components/EditDataPenjualan';
import 'flowbite'


ReactDOM.render(
  <BrowserRouter>
    <Routes>
      <Route path="/" element={<App />} />
      <Route path="add-data-penjualan" element={<CreateData />} />
      <Route path="data-penjualan" element={<DataPenjualan />} />
      <Route path="edit-data-penjualan/:id" element={<EditDataPenjualan />} />
    </Routes>
  </BrowserRouter>,
  document.getElementById('root')
);